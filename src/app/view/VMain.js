App.view.define("VMain", {
  extend: "Ext.Panel",
  alias: "widget.mainform",
  border: false,

  layout: "vbox",

  items: [
    {
      xtype: "button",
      itemId: "clickme",
      text: _("hello"),
      margin: 10,
    },
  ],
});
